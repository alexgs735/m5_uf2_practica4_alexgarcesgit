package Calculadora;

import java.util.Scanner;
//autor AlexG

/**
 * <h2>clase Calculadora, se utiliza para Sumar,restar,dividir y multiplicar numeros pasados por Scanner </h2>
 * 
 * @version 2-2022
 * @author AlexG
 * @since 24-2-2022
 */
public class LaCalculadora {

    static Scanner reader = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//definim dos variables int, aquestes variables emmagatzemaran els dos numeros implicats en les funcions de la calculadora
		int op1 = 0, op2 = 0;
		//Creem una variable de tipo char, aquesta variable emmagatzemara la opcio escollida per l'usuari en el selector de funcions
		char opcio;
		//Aquesta variable int, emmagatzemara el valor del resultat de les funcions de la calculadora
		int resultat = 0;
		//Aquesta variable boolean serveix per executar el programa mentre la variable sigui false
		boolean control = false;
		
		do {
			mostrar_menu();
			//es llegeix el caracter proporcionat per l'usuari
			opcio = llegirCar();
			//comen�a la execucio del selector del menu, depenent del caracter donat per l'usuari executara una funcio o un altre
			switch (opcio) {
                //Si el usuari escriu la O, seleccionara la funcio de suma
				case 'o':
                case 'O':
                	//Es demanan per pantalla i llegeixen els valors a sumar
                    op1 = llegirEnter();
                    op2 = llegirEnter();
                    control = true;
                    break;
                case '+':
                    if (control)
                    	//S'emmagatzema el valor de la suma a la variable resultat
                        resultat = suma(op1, op2);
                    //si la suma no es correcte o valida, mostra un missatge d'error
                    else mostrarError();
                    break;
                //Aquest case es tracta del definit per restar
                case '-':
                    if (control)
                    	//s'executa la funcio resta i es guarda el resultat a la variable resultat
                        resultat = resta(op1, op2);
                     	//si la resta no es correcte o no valida, mostra un missatge d'error
                    	else mostrarError();
                    break;
                    //Aquest case executa la funcio de multiplicacio
                case '*':
                    if (control)
                    	//S'executa la funcio de multiplicacio i es guarda el resultat a la variable resultat
                        resultat = multiplicacio(op1, op2);
                     	//Si la multiplicacio no es correcte o valida, es mostra un missatge d'error
                    	else mostrarError();
                    break;
                    //Aquest es el case que executa la funcio de divisio
                case '/':
                    if (control) {
                        //S'executa la funcio de divisio, s'emmagatzema el resultat a la variable resultat
                    	resultat = divisio(op1, op2);
                        //Aquest if serveix per comprovar que no estem dividint per un numero invalid
                    	if (resultat == -99)
                    		//si el numero no es valid, apareix el seguent missatge i el resultat no es mostra
                            System.out.print("No ha fet la divisi�");
                        	//en el cas de que la divisio sigui correcte es mostra el resultat
                    	else
                             visualitzar(resultat);
					}
		            else mostrarError();
                    break;
                case 'v':
                case 'V':
                    if (control)
                        visualitzar(resultat);
                     else mostrarError();
                    break;
                    //aquest case serveix per tancar el programa
                case 's':
                case 'S':
                    System.out.print("Acabem.....");

                    break;
                    // si no escollim una opcio valida al selector, ens apareix el missatge de opcio erronea
                default:
                    System.out.print("opci� erronia");
			}
			;
			
		} while (opcio != 's' && opcio != 'S');
		System.out.print("\nAdeu!!!");

	}

	/**
	 * Mostra un error si s'intenta dur a terme una operacio sense donar els numeros per fer-la
	 * @see LaCalculadora
	 * @param Mensaje de error
	 */
    public static void mostrarError() { /* procediment */
		System.out.println("\nError, cal introduir primer els valors a operar");
	}



    /**
	 * Suma dos digitos
	 * @see LaCalculadora
	 * @param res resultado de la operacion
	 * @param a primer numero a sumar
	 * @param b segundo numero a sumar
	 * @return resultado de la suma
	 */
	public static int suma(int a, int b) { /* funci� */
		int res;
		res = a + b;
		return res;
	}
	 /**
		 * Resta dos digitos
		 * @see LaCalculadora
		 * @param res resultado de la operacion
		 * @param a primer numero a restar
		 * @param b segundo numero a restar
		 * @return resultado de la resta
		 */
	
	public static int resta(int a, int b) { /* funci� */
		int res;
		res = a - b;
		return res;
	}

	 /**
		 * Multiplica dos digitos
		 * @see LaCalculadora
		 * @param res resultado de la operacion
		 * @param a primer numero a multiplicar
		 * @param b segundo numero a multiplicar
		 * @return resultado de la multiplicacion
		 */
	public static int multiplicacio(int a, int b) { /* funci� */
		int res;
		res = a * b;
		return res;
	}
	//AlexG
	 /**
		 * Divide dos digitos
		 * @see LaCalculadora
		 * @param res resultado de la operacion
		 * @param num1 numero a dividir
		 * @param num2 numero por el que dividir
		 * @return resultado de dividir
		 * @throws Mostra un error que indica que hi ha un error al dividir entre 0
		 */
	public static  int divideix(int num1, int num2) {
		int res;
	if (num2 == 0)
		throw new java.lang.ArithmeticException("Divisi� entre zero");
	else 
	res = num1/num2;
		return res;
	}
	
	/**
	 * Divide dos digitos
	 * @see LaCalculadora
	 * @param res resultado de la operacion
	 * @param a numero a dividir
	 * @param b numero por el que dividir
	 * @return resultado de dividir
	 */
	public static int divisio(int a, int b) { /* funci� */
		int res = -99;
		char op;

		do {
			System.out.println("M. " + a + " mod " + b);
			System.out.println("D  " + a + " / " + b);
			op = llegirCar();
			if (op == 'M' || op == 'm'){
                if (b == 0) 
                	
                    System.out.print("No es pot dividir entre 0\n");
                else
                    res = a % b;
			}

			else if (op == 'D' || op == 'd'){
                    if (b == 0)
                        System.out.print("No es pot dividir entre 0\n");
                    else
                        res = a / b;
                    }
                else
                    System.out.print("opci� incorrecte\n");
		} while (op != 'M' && op != 'm' && op != 'D' && op != 'd');

		return res;
	}
	/**
	 * Lee y almacena el caracter que representa una opcion en el selector
	 * @see LaCalculadora
	 * @param car caracter que escoge el usuario
	 * @param reader objeto de tipo lector que lee el cracter
	 * @return el caracter seleccionado por el usuario
	 */
	public static char llegirCar() // funci�
	{
		char car;

		System.out.print("Introdueix un car�cter: ");
		car = reader.next().charAt(0);

		//reader.nextLine();
		return car;
	}

	/**
	 * Lee un int dado por el usuario y confirma que es un entero
	 * @see LaCalculadora
	 * @param valor variable que almacena el entero
	 * @param valid variable que almacena si un numero es valido o no
	 * @param reader objeto que lee el numero dado por el usuario
	 * @return valor del numero dado por el usuario
	 * @throws Muestra un error al dar un numero no entero
	 */
	public static int llegirEnter() // funci�
	{
		int valor = 0;
		boolean valid = false;

		do {
			try {
				System.out.print("Introdueix un valor enter: ");
				valor = reader.nextInt();
				valid = true;
			} catch (Exception e) {
				System.out.print("Error, s'espera un valor enter");
				reader.nextLine();
			}
		} while (!valid);

		return valor;
	}
	/**
	 * Muestra el resultado de la operacion en un Sysout
	 * @see LaCalculadora
	 * @param res resultado de la operacion llevada a cabo
	 * @return resultado de la operacion
	 */
	public static void visualitzar(int res) { /* procediment */
		System.out.println("\nEl resultat de l'operacio �s " + res);
	}

	/**
	 * Divide dos digitos
	 * @see LaCalculadora
	 */
	public static void mostrar_menu() {
		System.out.println("\nCalculadora:\n");
		System.out.println("o.- Obtenir els valors");
		System.out.println("+.- Sumar");
		System.out.println("-.- Restar");
		System.out.println("*.- Multiplicar");
		System.out.println("/.- Dividir");
		System.out.println("v.- Visualitzar resultat");
		System.out.println("s.- Sortir");
		System.out.print("\n\nTria una opci� i ");
	}

}